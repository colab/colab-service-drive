# colab-service-drive

Colab service drive repo

### Informations

* Create a .env file and fill it with firebase information :

```env
    FIREBASE_API_KEY=
    FIREBASE_API_AUTH_DOMAIN=
    FIREBASE_API_DB_URL=
    FIREBASE_API_PROJECT_ID=
    FIREBASE_API_STORAGE=
    FIREBASE_API_MESSAGING=
    FIREBASE_API_APP_ID=
    MONGODB_URL=
```

### Documentation

* Find the swagger doc page by accessing [http://localhost:5000/swagger](http://localhost:5000/swagger)
