import User, { IUser } from '../models/user.model';

const list = async () => {
  return await User.find({});
};

const get = async (email: string) => {
  return await User.findOne({ email });
};

const create = async (user: IUser) => {
  return await new User(user).save();
};

const remove = async (id: String) => {
  return await User.findByIdAndDelete(id);
};

const update = async (id: String, user: IUser) => {
  return await User.findByIdAndUpdate(id, user, { new: true });
};

export { list, get, create, remove, update };
