import Evaluation, { IEvaluation } from '../models/evaluation.model';

const list = async (id: any) => {
  return await Evaluation.find({ classId: id }).populate('notes.studentId');
};

const get = async (id: String) => {
  return await Evaluation.findById(id).populate('notes.studentId');
};

const create = async (params: IEvaluation) => {
  return await new Evaluation(params).save();
};

const update = async (id: String, params: IEvaluation) => {
  return await Evaluation.findByIdAndUpdate(id, params, { new: true }).populate(
    'notes.studentId'
  );
};

const remove = async (id: String) => {
  return await Evaluation.findByIdAndDelete(id);
};

export { list, get, create, update, remove };
