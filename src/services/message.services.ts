import Message from '../models/message.model';
import { IMessage } from '../models/message.model';

const list = async (classId: any) => {
  return await Message.find({ classId }).populate('owner');
};

const create = async (params: IMessage) => {
  return await new Message(params).save();
};

export { list, create };
