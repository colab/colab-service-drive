import Class, { IClass } from '../models/class.model';

const list = async (id: any) => {
  let classes = await Class.find({ owner: id }).populate('children');
  let children = await Class.find({ children: { $in: [id] } });
  return [...classes, ...children];
};

const get = async (id: String) => {
  return await Class.findById(id).populate('children');
};

const create = async (params: IClass) => {
  return await new Class(params).save();
};

const update = async (id: String, params: IClass) => {
  return await Class.findByIdAndUpdate(id, params, { new: true }).populate(
    'children'
  );
};

const remove = async (id: String) => {
  return await Class.findByIdAndDelete(id).populate('children');
};

export { list, get, create, update, remove };
