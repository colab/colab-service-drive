import { Express } from 'express';
import { format } from 'util';
import admin from 'firebase-admin';
import Document, { IDocument } from '../models/document.model';

const bucket = admin.storage().bucket();

const create = (
  file: Express.Multer.File,
  classId: String,
  userId: String,
  parentId: String
) => {
  return new Promise((resolve, reject) => {
    if (!file) {
      reject('No image file');
    }
    let newFileName = `documents/${userId}/${classId}/${Date.now()}_${
      file.originalname
    }`;
    let fileUpload = bucket.file(newFileName);

    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype
      },
      resumable: false
    });

    blobStream.on('error', () => {
      reject('Something is wrong! Unable to upload at the moment.');
    });

    blobStream.on('finish', () => {
      // The public URL can be used to directly access the file via HTTP.
      const url = format(
        `https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`
      );
      const document = {
        name: file.originalname,
        url: url,
        type: file.mimetype,
        size: file.size,
        class: classId,
        owner: userId,
        parent: parentId,
        access: []
      };
      resolve(new Document(document).save());
    });

    blobStream.end(file.buffer);
  });
};

const createFolder = async (document: IDocument) => {
  return await new Document(document).save();
};

const remove = async (docs_id: any[]) => {
  return await Document.deleteMany({
    _id: {
      $in: docs_id
    }
  });
};

const get = async (id: String) => {
  return await Document.findById(id);
};
const list = async (parentId: any, classId?: any) => {
  return await Document.find({ parent: parentId, class: classId })
    .populate('class')
    .populate('owner');
};

const all = async () => {
  return await Document.find({});
};

const getTree = async (id: String, classId: String) => {
  const parent: any = await get(id);
  const items = [parent._id];
  if (parent.type === 'folder') {
    const children = await list(parent._id, classId);
    for (let child of children) {
      const values: any[] = await getTree(child._id, classId);
      values.map((obj) => {
        items.push(obj._id);
      });
    }
  }
  return items;
};

const update = async (id: String, doc: IDocument) => {
  return await Document.findByIdAndUpdate(id, doc, { new: true });
};

export { create, createFolder, remove, update, list, all, getTree };
