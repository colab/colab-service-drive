import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import * as evaluationService from '../services/evaluation.services';

const getEvaluation = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const object = await evaluationService.get(id);
    res.status(StatusCodes.OK).json(object);
  } catch (err) {
    res.status(StatusCodes.NOT_FOUND).json(err);
  }
};

const createEvaluation = async (req: Request, res: Response) => {
  try {
    const object = req.body;
    const newClass = await evaluationService.create(object);
    res.status(StatusCodes.CREATED).json(newClass);
  } catch (err) {
    res.status(StatusCodes.BAD_REQUEST).json(err);
  }
};

const updateEvaluation = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const object = req.body;
    const updatedClass = await evaluationService.update(id, object);
    res.status(StatusCodes.OK).json(updatedClass);
  } catch (err) {
    res.status(StatusCodes.NOT_MODIFIED).json(err);
  }
};

const deleteEvaluation = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const deletedClass = await evaluationService.remove(id);
    res.status(StatusCodes.OK).json(deletedClass);
  } catch (err) {
    res.status(StatusCodes.NOT_MODIFIED).json(err);
  }
};

export { getEvaluation, createEvaluation, updateEvaluation, deleteEvaluation };
