import { Request, Response, Express } from 'express';
import { StatusCodes } from 'http-status-codes';
import * as documentService from '../services/document.services';

const createDocument = async (req: Request, res: Response) => {
  if (req.body.type === 'folder') {
    try {
      const documents = await documentService.createFolder(req.body);
      res.status(StatusCodes.CREATED).json(documents);
    } catch (err) {
      res.status(StatusCodes.BAD_REQUEST).json(err);
    }
  } else {
    let files: any = req.files;
    let { class: classId, parent: parentId, owner: userId }: any = req.body;
    let promises = files.map((file: Express.Multer.File) =>
      documentService.create(file, classId, userId, parentId)
    );
    Promise.all(promises)
      .then((result) => {
        res.status(StatusCodes.CREATED).json(result);
      })
      .catch((err) => {
        res.status(StatusCodes.BAD_REQUEST).json(err);
      });
  }
};

const removeDocument = async (req: Request, res: Response) => {
  try {
    const classId = req.params.classId;
    const parentId = req.params.parentId;
    const documents_id = await documentService.getTree(parentId, classId);
    const documents = await documentService.remove(documents_id);
    res.status(StatusCodes.OK).json(documents);
  } catch (err) {
    res.status(StatusCodes.NOT_MODIFIED).json(err);
  }
};

const listByClassId = async (req: Request, res: Response) => {
  try {
    const classId = req.params.classId;
    const parentId = req.params.parentId;
    const documents = await documentService.list(parentId, classId);
    res.status(StatusCodes.OK).json(documents);
  } catch (err) {
    res.status(StatusCodes.BAD_REQUEST).json(err);
  }
};

const listAll = async (req: Request, res: Response) => {
  try {
    const documents = await documentService.all();
    res.status(StatusCodes.OK).json(documents);
  } catch (err) {
    res.status(StatusCodes.BAD_REQUEST).json(err);
  }
};

const tree = async (req: Request, res: Response) => {
  try {
    const classId = req.params.classId;
    const parentId = req.params.parentId;
    const documents = await documentService.getTree(parentId, classId);
    res.status(StatusCodes.OK).json(documents);
  } catch (err) {
    res.status(StatusCodes.BAD_REQUEST).json(err);
  }
};

const updateDocument = async (req: Request, res: Response) => {
  try {
    const documents = await documentService.update(req.params.id, req.body);
    res.status(StatusCodes.OK).json(documents);
  } catch (err) {
    res.status(StatusCodes.NOT_MODIFIED).json(err);
  }
};

export {
  createDocument,
  removeDocument,
  updateDocument,
  listByClassId,
  listAll,
  tree
};
