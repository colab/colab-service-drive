import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import * as messageService from '../services/message.services';

const createMessage = async (req: Request, res: Response) => {
  try {
    const object = req.body;
    const newClass = await messageService.create(object);
    res.status(StatusCodes.CREATED).json(newClass);
  } catch (err) {
    res.status(StatusCodes.BAD_REQUEST).json(err);
  }
};

export { createMessage };
