import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import * as classService from '../services/class.services';
import * as messageService from '../services/message.services';
import * as evaluationService from '../services/evaluation.services';

const listMessagesByClassId = async (req: Request, res: Response) => {
  try {
    const classId = req.params.id;
    const messages = await messageService.list(classId);
    res.status(StatusCodes.OK).json(messages);
  } catch (err) {
    res.status(StatusCodes.NOT_FOUND).json(err);
  }
};

const listEvaluationsByClassId = async (req: Request, res: Response) => {
  try {
    const classId = req.params.id;
    const object = await evaluationService.list(classId);
    res.status(StatusCodes.OK).json(object);
  } catch (err) {
    res.status(StatusCodes.NOT_FOUND).json(err);
  }
};

const getClass = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const object = await classService.get(id);
    res.status(StatusCodes.OK).json(object);
  } catch (err) {
    res.status(StatusCodes.NOT_FOUND).json(err);
  }
};
const createClass = async (req: Request, res: Response) => {
  try {
    const object = req.body;
    const newClass = await classService.create(object);
    res.status(StatusCodes.CREATED).json(newClass);
  } catch (err) {
    res.status(StatusCodes.BAD_REQUEST).json(err);
  }
};

const updateClass = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const object = req.body;
    const updatedClass = await classService.update(id, object);
    res.status(StatusCodes.OK).json(updatedClass);
  } catch (err) {
    res.status(StatusCodes.NOT_MODIFIED).json(err);
  }
};

const deleteClass = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const deletedClass = await classService.remove(id);
    res.status(StatusCodes.OK).json(deletedClass);
  } catch (err) {
    res.status(StatusCodes.NOT_MODIFIED).json(err);
  }
};

export {
  listMessagesByClassId,
  listEvaluationsByClassId,
  getClass,
  createClass,
  updateClass,
  deleteClass
};
