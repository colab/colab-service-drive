import { StatusCodes } from 'http-status-codes';
import { Request, Response } from 'express';
import * as userService from '../services/user.services';
import * as classService from '../services/class.services';

const listUsers = async (req: Request, res: Response) => {
  try {
    const user = await userService.list();
    res.status(StatusCodes.OK).json(user);
  } catch (err) {
    res.status(StatusCodes.NOT_FOUND).json(err);
  }
};

const listClassesByUserId = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const classes = await classService.list(id);
    res.status(StatusCodes.OK).json(classes);
  } catch (err) {
    res.status(StatusCodes.NOT_FOUND).json(err);
  }
};

const getUser = async (req: Request, res: Response) => {
  try {
    const user = await userService.get(req.params.email);
    res.status(StatusCodes.OK).json(user);
  } catch (err) {
    res.status(StatusCodes.NOT_FOUND).json(err);
  }
};

const createUser = async (req: Request, res: Response) => {
  try {
    const user = await userService.create(req.body);
    res.status(StatusCodes.CREATED).json(user);
  } catch (err) {
    console.log(err);
    res.status(StatusCodes.BAD_REQUEST).json(err);
  }
};

const updateUser = async (req: Request, res: Response) => {
  try {
    const user = await userService.update(req.params.id, req.body);
    res.status(StatusCodes.OK).json(user);
  } catch (err) {
    res.status(StatusCodes.NOT_MODIFIED).json(err);
  }
};

const deleteUser = async (req: Request, res: Response) => {
  try {
    const user = await userService.remove(req.params.id);
    res.status(StatusCodes.OK).json(user);
  } catch (err) {
    res.status(StatusCodes.NOT_MODIFIED).json(err);
  }
};

export {
  getUser,
  listUsers,
  listClassesByUserId,
  createUser,
  updateUser,
  deleteUser
};
