import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import swaggerUI from 'swagger-ui-express';
import cors from 'cors';
import helmet from 'helmet';
import * as dotenv from 'dotenv-flow';
dotenv.config();
import * as mongoose from './config/mongodb';
import * as firebase from './config/firebase';
import * as swaggerDocument from './config/swagger.json';
import classRouter from './routes/class.routes';
import userRouter from './routes/user.routes';
import messageRouter from './routes/message.routes';
import evaluationRouter from './routes/evaluation.routes';
firebase.init();
import documentRouter from './routes/document.routes';

// Init
const app = express();
mongoose.start();

// Config
app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/swagger', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

// Routes
app.get('/', (req: Request, res: Response) => {
  res.redirect('/swagger');
});
app.use('/classes', classRouter);
app.use('/users', userRouter);
app.use('/messages', messageRouter);
app.use('/documents', documentRouter);
app.use('/evaluations', evaluationRouter);

// Start
app.listen(process.env.PORT || 5000, () => console.log('Server running'));
