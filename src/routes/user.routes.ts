import express from 'express';
import * as userController from '../controllers/user.controllers';

const router = express.Router({ mergeParams: true });

router.get('/', userController.listUsers);
router.get('/:email', userController.getUser);
router.get('/:id/classes', userController.listClassesByUserId);
router.post('/', userController.createUser);
router.put('/:id', userController.updateUser);
router.delete('/:id', userController.deleteUser);

export default router;
