import express from 'express';
import * as messageController from '../controllers/message.controller';

const router = express.Router({ mergeParams: true });

router.post('/', messageController.createMessage);

export default router;
