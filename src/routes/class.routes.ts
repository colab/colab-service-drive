import express from 'express';
import * as classController from '../controllers/class.controllers';
import * as documentController from '../controllers/document.controllers';

const router = express.Router({ mergeParams: true });

router.get('/:id', classController.getClass);
router.get('/:id/messages', classController.listMessagesByClassId);
router.get('/:id/evaluations', classController.listEvaluationsByClassId);
router.post('/', classController.createClass);
router.put('/:id', classController.updateClass);
router.delete('/:id', classController.deleteClass);

export default router;
