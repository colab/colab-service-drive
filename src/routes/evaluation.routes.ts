import express from 'express';
import * as evaluationController from '../controllers/evaluation.controllers';

const router = express.Router({ mergeParams: true });
//classId = '5f803b771058362f9c4412bf';
router.get('/:id', evaluationController.getEvaluation);
router.post('/', evaluationController.createEvaluation);
router.put('/:id', evaluationController.updateEvaluation);
router.delete('/:id', evaluationController.deleteEvaluation);

export default router;
