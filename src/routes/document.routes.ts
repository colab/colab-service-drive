import express from 'express';
import Multer from 'multer';
import * as documentController from '../controllers/document.controllers';

const router = express.Router({ mergeParams: true });

const multer = Multer({ storage: Multer.memoryStorage() });

router.post('/', multer.any(), documentController.createDocument);
router.get('/:classId/:parentId', documentController.listByClassId);
router.get('/', documentController.listAll);
router.get('/:classId/children/:parentId/', documentController.tree);
router.delete(
  '/:classId/children/:parentId/',
  documentController.removeDocument
);
router.put('/:id', documentController.updateDocument);
export default router;
