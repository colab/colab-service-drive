import mongoose, { Schema, Document } from 'mongoose';

export interface IDocument extends Document {
  name: String;
  url: String;
  type: String;
  size: String;
  class: Schema.Types.ObjectId;
  owner: Schema.Types.ObjectId;
  parent: Schema.Types.ObjectId;
  access: [Schema.Types.ObjectId[]];
}

const DocumentSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    url: { type: String },
    type: { type: String, required: true },
    size: { type: String },
    class: { type: Schema.Types.ObjectId, ref: 'Class', required: true },
    owner: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    parent: { type: Schema.Types.ObjectId },
    access: [{ type: Schema.Types.ObjectId, ref: 'User' }]
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IDocument>('Document', DocumentSchema);
