import mongoose, { Schema, Document } from 'mongoose';
import { IUser } from './user.model';
import { IClass } from './class.model';

export interface IMessage extends Document {
  comment: String;
  owner: Schema.Types.ObjectId;
  classId: Schema.Types.ObjectId;
}

const messageSchema: Schema = new Schema(
  {
    comment: { type: String, required: true },
    owner: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    classId: { type: Schema.Types.ObjectId, required: true }
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IMessage>('Message', messageSchema);
