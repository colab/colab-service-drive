import mongoose, { Schema, Document } from 'mongoose';

export interface IEvaluation extends Document {
  name: string;
  coefficient: number;
  denominator: number;
  classId: Schema.Types.ObjectId;
  notes: [
    {
      studentId: Schema.Types.ObjectId;
      note: number;
    }
  ];
}
const evaluationSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    coefficient: { type: Number, required: true },
    denominator: { type: Number, required: true },
    classId: { type: Schema.Types.ObjectId, required: true },
    notes: [
      {
        studentId: { type: Schema.Types.ObjectId, ref: 'User' },
        note: { type: Number }
      }
    ]
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IEvaluation>('Note', evaluationSchema);
