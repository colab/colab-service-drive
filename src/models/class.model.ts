import mongoose, { Schema, Document } from 'mongoose';

export interface IClass extends Document {
  name: String;
  owner: Schema.Types.ObjectId;
  children: Schema.Types.ObjectId[];
}

const ClassSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    owner: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    children: [{ type: Schema.Types.ObjectId, ref: 'User' }]
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IClass>('Class', ClassSchema);
