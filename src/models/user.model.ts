import mongoose, { Schema, Document } from 'mongoose';

export interface IUser extends Document {
  email: String;
  firstname: String;
  lastname: String;
}

const userSchema: Schema = new Schema(
  {
    email: { type: String, required: true, unique: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true }
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IUser>('User', userSchema);
