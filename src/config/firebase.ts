import admin from 'firebase-admin';

const FIREBASE_PRIVATE_KEY: any = process.env.FIREBASE_PRIVATE_KEY;

const serviceAccount: any = {
  type: process.env.FIREBASE_TYPE,
  project_id: process.env.FIREBASE_PROJECT_ID,
  private_key_id: process.env.FIREBASE_PRIVATE_KEY_ID,
  private_key: FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
  client_email: process.env.FIREBASE_CLIENT_EMAIL,
  client_id: process.env.FIREBASE_CLIENT_ID,
  auth_uri: process.env.FIREBASE_AUTH_URI,
  token_uri: process.env.FIREBASE_TOKEN_URI,
  auth_provider_x509_cert_url: process.env.FIREBASE_PROVIDER,
  client_x509_cert_url: process.env.FIREBASE_CLIENT
};

function init(): void {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: 'colab-drive-app.appspot.com'
  });
}

export { init };
