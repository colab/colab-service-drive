import mongoose from 'mongoose';

// const url: any = process.env.MONGODB_URL;

function start(): void {
  mongoose
    .connect(
      'mongodb+srv://colab:UCa5bxTJNCJx6H79@cluster0.yfjia.gcp.mongodb.net/colab?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
      }
    )
    .then(() => console.log('Connected to mongoDB'))
    .catch((e: any) => console.log(e));
}

function stop(): void {
  mongoose.connection.close();
}
export { start, stop };
